package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JenkinsLoginHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public JenkinsLoginHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "#j_username")
	WebElement username;
	@FindBy(css = "div.formRow:nth-child(2) > input:nth-child(1)")
	WebElement password;
	@FindBy(css = ".submit-button")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

	public void VerifyJenkinsLogin() throws InterruptedException {
		String heading = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".breadcrumbBarAnchor"))).getText();
		if (heading.equals("Dashboard")) {
			System.out.println("Login to Jenkins is Successful");
		} else {
			System.out.println("Login to Jenkins is Failed");
		}
		Thread.sleep(1500);

	}

	public void LogoutJenkins() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".login > a:nth-child(3)"))).click();
		Thread.sleep(1500);
		driver.close();
	}

}
