package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RDWebaccessLoginHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public RDWebaccessLoginHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "DomainUserName")
	WebElement username;
	@FindBy(id = "UserPass")
	WebElement password;
	@FindBy(id = "btnSignIn")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

	public void VerifyLoginRD() throws InterruptedException {
		String heading = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PORTAL_REMOTE_PROGRAMS")))
				.getText();
		if (heading.equals("RemoteApp and Desktops")) {
			System.out.println("Login to RD WebAccess is Successful");
		} else {
			System.out.println("Login to RD WebAccess is Failed");
		}
		Thread.sleep(1500);
	}

	public void LogoutRD() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PORTAL_SIGNOUT"))).click();
		Thread.sleep(1500);
		driver.close();
	}

}
