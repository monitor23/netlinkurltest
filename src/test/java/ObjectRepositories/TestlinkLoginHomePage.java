package ObjectRepositories;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestlinkLoginHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public TestlinkLoginHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "tl_login")
	WebElement username;
	@FindBy(id = "tl_password")
	WebElement password;
	@FindBy(id = "tl_login_button")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

	public void VerifyLoginTestlink() throws InterruptedException {
		int frameIndex = 0;
		List<WebElement> listFrames = driver.findElements(By.tagName("iframe"));
		// System.out.println("list frames " + listFrames.size());
		driver.switchTo().frame(listFrames.get(frameIndex));
		String uname = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".bold"))).getText();
		// System.out.println(uname);
		if (uname.equals("Albin [tester]")) {
			System.out.println("Login to Testlink is Successful");
		} else {
			System.out.println("Login to Testlink is Failed");
		}
		Thread.sleep(1500);
	}

	public void LogoutTestlink() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.cssSelector(".menu_title > span:nth-child(2) > a:nth-child(2) > img:nth-child(1)"))).click();
		Thread.sleep(1500);
		driver.switchTo().defaultContent();
		driver.close();

	}

}
