package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RedmineLoginHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public RedmineLoginHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "username")
	WebElement username;
	@FindBy(id = "password")
	WebElement password;
	@FindBy(id = "login-submit")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

	public void VerifyLoginRedmine() throws InterruptedException {
		String heading = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#content > h2:nth-child(1)")))
				.getText();
		// System.out.println(heading);
		if (heading.equals("Home")) {
			System.out.println("Login to Redmine is Successful");
		} else {
			System.out.println("Login to Redmine is Failed");
		}
		Thread.sleep(1500);
	}

	public void LogoutRedmine() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".logout"))).click();
		Thread.sleep(1500);
		driver.close();

	}

}
