package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.TestlinkLoginHomePage;

public class TestlinkDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://testlink.netlink-testlabs.com");

	}

	@Test
	public void TestlinkLogin() throws InterruptedException {
		TestlinkLoginHomePage tlhp = new TestlinkLoginHomePage(driver);
		tlhp.WaitFunction();
		tlhp.Username().sendKeys("albin");
		tlhp.Password().sendKeys("abcd1234");
		tlhp.Login().click();
		tlhp.VerifyLoginTestlink();
		tlhp.LogoutTestlink();
	}

}
