package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.JenkinsLoginHomePage;

public class JenkinsDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://jenkins.netlink-testlabs.com/");

	}

	@Test
	public void JenkinsLogin() throws InterruptedException {
		JenkinsLoginHomePage jlhp = new JenkinsLoginHomePage(driver);
		jlhp.WaitFunction();
		jlhp.Username().sendKeys("Selenium");
		jlhp.Password().sendKeys("Password1!");
		jlhp.Login().click();
		jlhp.VerifyJenkinsLogin();
		jlhp.LogoutJenkins();
	}

}
