package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.RedmineLoginHomePage;

public class RedmineDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://redmine.netlink-testlabs.com/");

	}

	@Test
	public void RedmineLogin() throws InterruptedException {
		RedmineLoginHomePage rlhp = new RedmineLoginHomePage(driver);
		rlhp.WaitFunction();
		rlhp.Username().sendKeys("albin");
		rlhp.Password().sendKeys("abcd1234");
		rlhp.Login().click();
		rlhp.VerifyLoginRedmine();
		rlhp.LogoutRedmine();
	}

}
