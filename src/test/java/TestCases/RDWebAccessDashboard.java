package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.RDWebaccessLoginHomePage;

public class RDWebAccessDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://remoteapp.netlink-testlabs.com");

	}

	@Test
	public void RDWebAccessLogin() throws InterruptedException {
		RDWebaccessLoginHomePage rdlhp = new RDWebaccessLoginHomePage(driver);
		rdlhp.WaitFunction();
		rdlhp.Username().sendKeys("ad\\selenium");
		rdlhp.Password().sendKeys("Password1!");
		rdlhp.Login().click();
		rdlhp.VerifyLoginRD();
		rdlhp.LogoutRD();
	}

}
